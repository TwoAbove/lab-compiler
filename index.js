const fs = require('fs');
const path = require('path');
const args = require('yargs').argv;
const debug = require('debug');

const log = debug('log');

log(args);

const keywords = ['print'];

const isAlphaNumeric = ch => ch.match(/^[a-z]+[a-z0-9]*$/i);
const isSpace = ch => ch.length === 1 && ch.match(/^[ \t]+$/i);
const isNewline = ch => ch.length === 1 && ch.match(/^[\r\n]+$/i);
const isPunctuation = ch => ch.length === 1 && ch.match(/^[();=+/\-*]+$/i);
const isNum = ch => ch.match(/^[0-9]+$/i);
const isKeyword = ch => keywords.includes(ch);

class Lexer {
  constructor(input) {
    this.input = input;
    this.output = [];

    this.optable = {
      '+': 'PLUS',
      '-': 'MINUS',
      '*': 'MULTIPLY',
      '/': 'DIVIDE',
      ';': 'SEMI',
      '(': 'L_PAREN',
      ')': 'R_PAREN',
      '<': 'L_ANG',
      '>': 'R_ANG',
      '=': 'EQUALS',
    };
  }

  getType(token) {
    if (isPunctuation(token)) {
      return this.optable[token];
    }
    if (isNum(token)) {
      return 'NUMBER';
    }
    if (isKeyword(token)) {
      return 'KEYWORD';
    }
    if (isAlphaNumeric(token)) {
      return 'IDENTIFIER';
    }
  }

  skipNonTokens() {
    while (this.n < this.file.length) {
      const c = this.currentChar;
      if (isSpace(c)) {
        this.step();
      } else if (isNewline(c)) {
        this.col = 1;
        this.line++;
        this.n++;
      } else {
        break;
      }
    }
  }

  step() {
    this.col++;
    this.n++;
  }

  get currentChar() {
    return this.file.charAt(this.n);
  }

  getToken() {
    let token = '';
    if (isPunctuation(this.currentChar)) {
      token = this.currentChar;
      this.step();
      return token;
    }
    while (!isSpace(this.currentChar) && !isPunctuation(this.currentChar)) {
      token += this.currentChar;
      this.step();
    }
    return token;
  }

  getNextToken() {
    this.skipNonTokens();
    const token = this.getToken();
    const type = this.getType(token);
    const out = { token, type, line: this.line, col: this.col };
    if (!type) {
      throw new Error(`Unexpected token at ${out.line}:${out.col}`);
    }
    return out;
  }

  log() {
    log(this.output);
  }

  lex() {
    this.n = 0;
    this.col = 1;
    this.line = 1;
    const pathtoInput = path.resolve(process.cwd(), this.input);
    log(pathtoInput);
    this.file = fs.readFileSync(pathtoInput, 'utf-8');
    while (this.n < this.file.length - 1) {
      const token = this.getNextToken();
      this.output.push(token);
    }
  }
}

class Node {
  constructor(left, op, right) {
    this.left = left;
    this.op = op;
    this.right = right;
  }
}

class Transpiler {
  constructor(tokens) {
    this.tokens = tokens;
    this.i = 0;

    this.variables = [];
    this.consts = [];
    this.keywords = [];
    this.actions = [];
    this.ops = [];
  }

  get currentToken() {
    return this.tokens[this.i];
  }

  get nextToken() {
    return this.tokens[this.i + 1];
  }

  consume(type) {
    const token = this.currentToken;
    if (token.type !== type) {
      this.error(`Unexpected token. Wanted ${type}, got `);
    }
    this.i++;
    return token;
  }

  getTable(token) {
    switch (token.type) {
      case 'IDENTIFIER':
        return this.variables;
      case 'NUMBER':
        return this.consts;
      case 'KEYWORD':
        return this.keywords;
      case 'PLUS':
      case 'MINUS':
      case 'MULTIPLY':
      case 'DIVIDE':
      case 'EQUALS':
        return this.ops;
    }
  }

  getTableName(token) {
    switch (token.type) {
      case 'IDENTIFIER':
        return 'T_VAR';
      case 'NUMBER':
        return 'T_CONSTS';
      case 'KEYWORD':
        return 'T_KW';
      case 'PLUS':
      case 'MINUS':
      case 'MULTIPLY':
      case 'DIVIDE':
      case 'EQUALS':
        return 'T_OPS';
    }
  }

  addToTable(token) {
    const table = this.getTable(token);
    const pos = table.indexOf(token.token);
    if (pos === -1) {
      table.push(token.token);
    }
  }

  getFromTable(token) {
    const table = this.getTable(token);
    const pos = table.indexOf(token.token);
    if (pos > -1) {
      return pos;
    }
    this.error('Token is not defined: ', token);
  }

  /*
    expression: term ((PLUS | MINUS) term)* 
  */
  expression() {
    let node = this.term();
    while (['PLUS', 'MINUS'].includes(this.currentToken.type)) {
      const token = this.currentToken;
      if (token.type === 'PLUS') {
        this.consume('PLUS');
      } else if (token.type === 'MINUS') {
        this.consume('MINUS');
      }
      node = new Node(node, token, this.term());
    }
    return node;
  }

  /*
    term : factor ((MUL | DIV) factor)*
  */
  term() {
    let node = this.factor();
    while (['MULTIPLY', 'DIVIDE'].includes(this.currentToken.type)) {
      const token = this.currentToken;
      if (token.type === 'MULTIPLY') {
        this.consume('MULTIPLY');
      } else if (token.type === 'DIVIDE') {
        this.consume('DIVIDE');
      }
      node = new Node(node, token, this.factor());
    }
    return node;
  }

  /*
  factor: variable
          | PLUS factor
          | MINUS factor
          | INTEGER
          | LPAREN expr RPAREN
  */
  factor() {
    const token = this.currentToken;
    switch (token.type) {
      case 'PLUS':
        this.consume('PLUS');
        return this.factor();
      case 'MINUS':
        this.consume('MINUS');
        return this.factor();
      case 'NUMBER':
        return this.consume('NUMBER');
      case 'L_PAREN':
        this.consume('L_PAREN');
        const node = this.expression();
        this.consume('R_PAREN');
        return node;
      default:
        return this.consume('IDENTIFIER');
    }
  }

  assignment() {
    const id = this.consume('IDENTIFIER');
    const eq = this.consume('EQUALS');
    const val = this.expression();
    return new Node(id, eq, val);
  }

  consumeAction() {
    const token = this.consume('KEYWORD');
    if (token.token === 'print') {
      let pos = this.keywords.indexOf(token.token);
      if (pos === -1) {
        this.keywords.push(token.token);
        pos = this.keywords.length - 1;
      }
      const val = this.expression();
      return new Node(null, token, val);
    }
    this.error('Unexpected token');
  }

  statement() {
    let action;
    const token = this.currentToken;
    if (token.type === 'IDENTIFIER') {
      action = this.assignment();
    }
    if (token.type === 'KEYWORD') {
      action = this.consumeAction();
    }
    return action;
  }

  transpile() {
    let action = this.statement();
    this.actions.push(action);
    while (this.currentToken.type === 'SEMI') {
      this.consume('SEMI');
      if (!this.currentToken) return;
      action = this.statement();
      this.actions.push(action);
    }
    if (this.currentToken.type === 'IDENTIFIER') {
      this.error('Unexpected keyword');
    }
  }

  error(message, token) {
    if (!token) {
      token = this.currentToken;
    }
    throw new Error(`${message} ${token.token} at ${token.line}:${token.col}`);
  }

  checkSemanticsRecursive(node) {
    if (['EQUALS'].includes(node.op.type)) {
      if (node.left.type !== 'IDENTIFIER') {
        this.error('Cannot assign to ', node.left);
      }
      if (node.right instanceof Node) {
        this.checkSemanticsRecursive(node.right);
      } else if (node.right.type === 'NUMBER') {
        this.addToTable(node.right);
      }
      this.addToTable(node.left);
    }
    if (['PLUS', 'MINUS', 'MULTIPLY', 'DIVIDE'].includes(node.op.type)) {
      if (node.right.type === 'IDENTIFIER') {
        this.getFromTable(node.right);
      }
      if (node.right.type === 'NUMBER') {
        this.addToTable(node.right);
      }
      if (node.right instanceof Node) {
        this.checkSemanticsRecursive(node.right);
      }
      if (node.left instanceof Node) {
        this.checkSemanticsRecursive(node.left);
      }
    }
    this.addToTable(node.op);
  }

  checkSemantics() {
    for (const i in this.actions) {
      this.checkSemanticsRecursive(this.actions[i]);
    }
  }

  log() {
    log('Variables');
    log(this.variables);
    log('Consts');
    log(this.consts);
    log('Keywords');
    log(this.keywords);
    log('Operators');
    log(this.ops);
    log('Actions');
    log(this.actions);
  }
}

class TranspilerToPosk {
  constructor(transpiler) {
    this.transpiler = transpiler;
    this.output = [[]];
    this.i = 0;
  }

  strip(token) {
    const tableName = this.transpiler.getTableName(token);
    const tableVal = this.transpiler.getFromTable(token);
    return `${tableName}:${tableVal}`;
  }

  get currentAction() {
    return this.output[this.i];
  }

  toPolskRecursive(action) {
    if (action.left instanceof Node) {
      this.toPolskRecursive(action.left);
    } else if (action.left) {
      const val = this.strip(action.left);
      this.currentAction.push(val);
    }
    if (action.right instanceof Node) {
      this.toPolskRecursive(action.right);
    } else {
      const val = this.strip(action.right);
      this.currentAction.push(val);
    }
    const val = this.strip(action.op);
    this.currentAction.push(val);
  }

  log() {
    log('Polsk Notation: ');
    this.output.map(out => log(out.join(',')));
  }

  toPolsk() {
    for (const i in this.transpiler.actions) {
      this.toPolskRecursive(this.transpiler.actions[i]);
      this.i++;
      this.output[this.i] = [];
    }
  }
}

const lexer = new Lexer(args.i);
lexer.lex();
lexer.log();
const transpiler = new Transpiler(lexer.output);
transpiler.transpile();
transpiler.checkSemantics();
transpiler.log();
const polsk = new TranspilerToPosk(transpiler);
polsk.toPolsk();
polsk.log();
